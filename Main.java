package text_statistics;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Здравствуй уважаемый пользователь!\n Данная программа производит подсчёт символов " +
                "в неформатированном текстовом документе. Введите путь к файлу:");
        String path = scanner.nextLine();

        try {
            TextStatistics textStatistics=new TextStatistics(path);
            System.out.println("Кол-во символов: "+textStatistics.characterCount());
            System.out.println("Кол-во символов без пробелов: "+textStatistics.characterCountWithoutSpaces());
            System.out.println("Кол-во строк: "+(textStatistics.countLines()+1));
            System.out.println("Кол-во слов: "+textStatistics.countWord());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}