package text_statistics;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextStatistics {
    private String text = "";
    public TextStatistics(String path) throws IOException {//ошибка ввода/вывода
        BufferedReader bufferedReader=new BufferedReader( new FileReader(path));
        String line;
        while ((line=bufferedReader.readLine()) != null){
            text+=line;
        }
        bufferedReader.close();
    }
    /*
     * Метод по подсчёту символов с пробелов
     */
    public int characterCount(){
        return countPattern("[^\t\n\r]");
    }
    /*
     * Метод по подсчёту символов без пробела
     */
    public int characterCountWithoutSpaces(){
        return countPattern("[^\t\n\r ]");
    }
    /*
    * Метод по подсчёту строк
    */
    public int countLines(){
        return countPattern("[\n]");
    }
    public int countWord(){
        return countPattern("[А-Яа-яA-Za-z0-9]+|[А-Яа-яA-Za-z0-9]+\\-[А-Яа-яA-Za-z0-9]+");
    }
    private int countPattern(String s) {
        Pattern pattern= Pattern.compile(s);
        Matcher matcher= pattern.matcher(text);
        int count = 0;
        for (; matcher.find(); count++);
        return count;
    }


}
